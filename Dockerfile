FROM alpine

LABEL maintainer="Ricardo Pereira"

run mkdir cv

COPY cv.txt /cv

WORKDIR /cv

CMD ["cat", "/cv/cv.txt"]
